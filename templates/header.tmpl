{{define "header"}}
<!DOCTYPE html>
<html lang="{{ PGettext "Language Identifier" "en" }}" class="codeberg-design">
<head>
	<!-- Meta tags -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	<meta name="viewport" content="width=device-width" />

	<!-- Favicon and title -->
	<title>{{ PGettext "Page Title" "Join Codeberg e. V." }}</title>

	<!-- Codeberg Design -->
	<link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.svg" type="image/svg+xml" />
	<link rel="apple-touch-icon" href="https://design.codeberg.org/logo-kit/apple-touch-icon.png" />

	<link rel="stylesheet" href="https://design.codeberg.org/design-kit/codeberg.css" />

    <!-- <link rel="stylesheet" href="//codeberg.org/assets/css/index.css?v=1.19.3~153-gdbaae93"> -->
    <!-- <link rel="stylesheet" href="//codeberg.org/assets/css/theme-codeberg-auto.css?v=1.19.3~153-gdbaae93"> -->
    
    <script defer src="https://design.codeberg.org/design-kit/codeberg.js"></script>

	<link href="https://fonts.codeberg.org/dist/inter/Inter%20Web/inter.css" rel="stylesheet" />
	<link href="https://fonts.codeberg.org/dist/fontawesome6/css/all.min.css" rel="stylesheet" />

</head>
<body
	class="with-custom-webkit-scrollbars with-custom-css-scrollbars"
	data-dm-shortcut-enabled="true" data-sidebar-shortcut-enabled="true"
	data-set-preferred-theme-onload="true">

	<style type="text/css" media="screen">
		.codeberg-design .content {
			max-width: 120ch;
			margin-left: auto;
			margin-right: auto;
		}
		.codeberg-design .content-wrapper {
			padding-left: 20px;
			padding-right: 20px;
		}
		.codeberg-design label.required::after {
			margin-left: unset;
		}
		[hidden] { display: none !important; }
	</style>

	<div class="page-wrapper with-navbar with-sidebar" data-sidebar-type="overlayed-sm-and-down">
		{{ block "nav" . }}{{ end }}

		{{ block "sidebar" . }}{{ end }}
		<div class="content-wrapper">
{{end}}

{{ define "nav" }}
	<nav class="navbar">
		<div class="navbar-content">
			<button class="btn btn-primary" type="button" onclick="halfmoon.toggleSidebar()">
				<i class="fa fa-bars" aria-hidden="true"></i>
				<span class="sr-only">Toggle sidebar</span>
			</button>
		</div>

		<a href="/" class="navbar-brand" title="Join Codeberg">
			<img src="https://design.codeberg.org/logo-kit/icon_inverted.svg" alt="Codeberg">
			Join
		</a>

		<ul class="navbar-nav d-none d-md-flex w-full">
			<li class="nav-item">
				<a href="https://codeberg.org" class="nav-link">Codeberg</a>
			</li>
			<li class="nav-item">
				<a href="https://blog.codeberg.org" class="nav-link">Blog</a>
			</li>
			<li class="nav-item">
				<a href="https://docs.codeberg.org" class="nav-link">Documentation</a>
			</li>
			<li class="nav-item ml-auto">
				<a href="javascript:;" onclick="toggleDarkMode()" class="nav-link text-center">
					<i class="fa fa-moon"></i>
				</a>
			</li>
		</ul>

		<div class="navbar-content d-md-none ml-auto">
			<div class="dropdown with-arrow">
				<button class="btn" data-toggle="dropdown" type="button" id="navbar-dropdown-toggle-btn-1">
					Menu <i class="fa fa-angle-down" aria-hidden="true"></i>
				</button>
				<div class="dropdown-menu dropdown-menu-right w-200" aria-labelledby="navbar-dropdown-toggle-btn-1">
					<a href="/" class="dropdown-item">Home</a>
					<div class="dropdown-divider my-5"></div>
					<a href="https://codeberg.org" class="dropdown-item">Codeberg</a>
					<a href="https://blog.codeberg.org" class="dropdown-item">Blog</a>
					<a href="https://docs.codeberg.org" class="dropdown-item">Documentation</a>
					<div class="dropdown-divider my-5"></div>
					<a href="https://social.anoxinon.de/@Codeberg" class="dropdown-item">
						<i class="fab fa-mastodon" aria-hidden="true"></i> Mastodon
					</a>
					<a href="https://matrix.to/#/#codeberg-space:matrix.org" class="dropdown-item">
						<svg fill="currentColor" viewBox="0 0 5.3651 5.3652" role="img" aria-hidden="true" focusable="false" width="16" height="16">
										<path d="m.1552.0001a.1551.1551 0 00-.155.155l0 5.055a.1551.1551 0 00.155.155l.4806 0a.1551.1551 0 00.155-.155l0-.1147a.1551.1551 0 00-.155-.155l-.1938 0 0-4.515.1938 0a.1551.1551 0 00.155-.155l0-.1152a.1551.1551 0 00-.155-.155l-.4806 0zm4.5749 0a.1551.1551 0 00-.1555.155l0 .1152a.1551.1551 0 00.1555.155l.1938 0 0 4.515-.1938 0a.1551.1551 0 00-.1555.155l0 .1147a.1551.1551 0 00.1555.155l.4801 0a.1551.1551 0 00.155-.155l0-5.055a.1551.1551 0 00-.155-.155l-.4801 0zm-2.4014 1.5963c-.1428.0003-.2761.0304-.3902.0956-.0158.009-.0287.0221-.0439.032a.1551.1551 0 00-.1235-.0785l-.4672 0a.1551.1551 0 00-.155.1555l0 1.8035a.1551.1551 0 00.155.155l.4966 0a.1551.1551 0 00.155-.155l0-1.047c0-.0786.013-.1295.0212-.1468.0191-.0398.0379-.062.0501-.0718a.1551.1551 0 00.0005-.0005c.0233-.0192.0398-.0257.0491-.0279a.1551.1551 0 00.0021-.0005c.0392-.01.0691-.0119.0543-.0119.0644 0 .0964.0142.0951.0134a.1551.1551 0 00.001.001c.0219.0142.0252.0214.0279.0289a.1551.1551 0 00.0005.0021c.0116.0306.0181.0615.0196.0961a.1551.1551 0 000 .0031c.002.0284.0031.0805.0031.1514l0 1.0108a.1551.1551 0 00.155.155l.4935 0a.1551.1551 0 00.1556-.155l0-1.0041c0-.0252.0038-.0783.0119-.1499a.1551.1551 0 00.0005-.0036c.002-.0198.01-.0474.03-.0832a.1551.1551 0 00.0021-.0041c.008-.015.0193-.029.046-.0455a.1551.1551 0 00.001-.001c.008-.005.0345-.0155.0853-.0155.0567 0 .0863.0117.0858.0114a.1551.1551 0 00.001.0005c.0191.011.0251.0192.031.031a.1551.1551 0 00.001.0031c.0152.029.0206.0519.0217.0734a.1551.1551 0 00.0005.0021c.004.0682.0057.1225.0057.1612l0 1.0237a.1551.1551 0 00.155.155l.494 0a.1551.1551 0 00.155-.155l0-1.2077c0-.1328-.0182-.2526-.061-.3581-.0404-.0997-.0988-.1873-.1757-.2553-.0767-.0681-.1682-.1174-.2682-.1452-.0995-.028-.203-.0419-.3085-.0419-.1493 0-.2903.0369-.4093.1132-.0417.0265-.0786.0579-.1147.0899-.0484-.052-.1046-.0944-.169-.1235a.1551.1551 0 00-.001 0c-.1176-.0522-.244-.0793-.3736-.0796l-.0021 0z"></path>
									</svg> Matrix
					</a>
					<div class="dropdown-divider my-5"></div>
					<a href="javascript:;" onclick="toggleDarkMode()" class="dropdown-item">
						<i class="fa fa-moon"></i>&nbsp; Toggle Dark Mode
					</a>
				</div>
			</div>
		</div>
	</nav>
{{ end }}

{{ define "sidebar" }}
	<div class="sidebar-overlay" onclick="halfmoon.toggleSidebar()"></div>
	<div class="sidebar">
		<div class="sidebar-menu">
			<div class="sidebar-title">Navigation</div>
			<div class="sidebar-divider"></div>
			<a class="sidebar-link" href="#ch-membership">{{ Gettext "Membership Type" }}</a>
			<a class="sidebar-link" href="#ch-volunteer-work">{{ Gettext "Volunteer Work" }}</a>
			<a class="sidebar-link" href="#ch-personal">{{ Gettext "Personal Information" }}</a>
			<a class="sidebar-link" href="#ch-fee">{{ Gettext "Membership Fee" }}</a>

			<br>
			<div class="sidebar-title">{{ Gettext "Socials" }}</div>
			<div class="sidebar-divider"></div>
			<a href="https://social.anoxinon.de/@Codeberg" class="sidebar-link">
				<i class="fab fa-mastodon pr-5" aria-hidden="true"></i> Mastodon
			</a>	
			<a href="https://matrix.to/#/#codeberg-space:matrix.org" class="sidebar-link">
				<svg fill="currentColor" viewBox="0 0 5.3651 5.3652" role="img" aria-hidden="true" focusable="false" width="16" height="16">
								<path d="m.1552.0001a.1551.1551 0 00-.155.155l0 5.055a.1551.1551 0 00.155.155l.4806 0a.1551.1551 0 00.155-.155l0-.1147a.1551.1551 0 00-.155-.155l-.1938 0 0-4.515.1938 0a.1551.1551 0 00.155-.155l0-.1152a.1551.1551 0 00-.155-.155l-.4806 0zm4.5749 0a.1551.1551 0 00-.1555.155l0 .1152a.1551.1551 0 00.1555.155l.1938 0 0 4.515-.1938 0a.1551.1551 0 00-.1555.155l0 .1147a.1551.1551 0 00.1555.155l.4801 0a.1551.1551 0 00.155-.155l0-5.055a.1551.1551 0 00-.155-.155l-.4801 0zm-2.4014 1.5963c-.1428.0003-.2761.0304-.3902.0956-.0158.009-.0287.0221-.0439.032a.1551.1551 0 00-.1235-.0785l-.4672 0a.1551.1551 0 00-.155.1555l0 1.8035a.1551.1551 0 00.155.155l.4966 0a.1551.1551 0 00.155-.155l0-1.047c0-.0786.013-.1295.0212-.1468.0191-.0398.0379-.062.0501-.0718a.1551.1551 0 00.0005-.0005c.0233-.0192.0398-.0257.0491-.0279a.1551.1551 0 00.0021-.0005c.0392-.01.0691-.0119.0543-.0119.0644 0 .0964.0142.0951.0134a.1551.1551 0 00.001.001c.0219.0142.0252.0214.0279.0289a.1551.1551 0 00.0005.0021c.0116.0306.0181.0615.0196.0961a.1551.1551 0 000 .0031c.002.0284.0031.0805.0031.1514l0 1.0108a.1551.1551 0 00.155.155l.4935 0a.1551.1551 0 00.1556-.155l0-1.0041c0-.0252.0038-.0783.0119-.1499a.1551.1551 0 00.0005-.0036c.002-.0198.01-.0474.03-.0832a.1551.1551 0 00.0021-.0041c.008-.015.0193-.029.046-.0455a.1551.1551 0 00.001-.001c.008-.005.0345-.0155.0853-.0155.0567 0 .0863.0117.0858.0114a.1551.1551 0 00.001.0005c.0191.011.0251.0192.031.031a.1551.1551 0 00.001.0031c.0152.029.0206.0519.0217.0734a.1551.1551 0 00.0005.0021c.004.0682.0057.1225.0057.1612l0 1.0237a.1551.1551 0 00.155.155l.494 0a.1551.1551 0 00.155-.155l0-1.2077c0-.1328-.0182-.2526-.061-.3581-.0404-.0997-.0988-.1873-.1757-.2553-.0767-.0681-.1682-.1174-.2682-.1452-.0995-.028-.203-.0419-.3085-.0419-.1493 0-.2903.0369-.4093.1132-.0417.0265-.0786.0579-.1147.0899-.0484-.052-.1046-.0944-.169-.1235a.1551.1551 0 00-.001 0c-.1176-.0522-.244-.0793-.3736-.0796l-.0021 0z"></path>
							</svg> Matrix
			</a>

			<br>
			<div class="sidebar-title">{{ Gettext "Legal" }}</div>
			<div class="sidebar-divider"></div>
			<a href="https://codeberg.org/codeberg/org/src/en/bylaws.md" class="sidebar-link">{{ Gettext "Bylaws" }}</a>
			<a href="https://codeberg.org/codeberg/org/src/TermsOfUse.md" class="sidebar-link">{{ Gettext "Terms of Use" }}</a>
			<a href="https://codeberg.org/codeberg/org/src/PrivacyPolicy.md" class="sidebar-link">{{ Gettext "Privacy Policy" }}</a>
			<a href="https://codeberg.org/codeberg/org/src/Imprint.md" class="sidebar-link">{{ Gettext "Imprint" }}</a>
		</div>
	</div>
{{ end }}

{{ define "script" }}
	<script>
		function toggleDarkMode(){
			halfmoon.toggleDarkMode();

			var fa = document.getElementsByClassName("fa");
			for (var i = 0; i < fa.length; i++) {
				if (fa[i].classList.contains('fa-moon') || fa[i].classList.contains('fa-sun')) {
					fa[i].classList.toggle('fa-sun');
					fa[i].classList.toggle('fa-moon');
				}
			}
		}

		function updateConditionalForms() {
			const val = id => (e => (e.getAttribute("type") === "checkbox" || e.getAttribute("type") === "radio") ? e.checked : e.value)(
					document.getElementById(id) ||
					[...document.getElementsByName(id)].filter(e => e.hasAttribute("checked"))[0] ||
					document.getElementsByName(id)[0]
			);
			const fmt = (num, digits, precision) => Number(num)
					.toFixed(precision)
					.replace(/^[^.]*/, x => x.padStart(digits, "0"));
			[...document.querySelectorAll("[hidden-if]")].forEach(e => {
				e.hidden = eval(e.getAttribute("hidden-if"));
			});
			[...document.querySelectorAll("[disabled-if]")].forEach(e => {
				e.disabled = eval(e.getAttribute("disabled-if"));
				if (e.disabled && e.parentElement.tagName === "SELECT" && e.selected) {
					e.parentElement.selectedIndex = -1;
				}
			});
			[...document.querySelectorAll("[required-if]")].forEach(e => {
				if (e.tagName === "LABEL") {
					if (eval(e.getAttribute("required-if"))) e.classList.add("required");
					else e.classList.remove("required");
				} else {
					e.required = eval(e.getAttribute("required-if"));
				}
			});
			[...document.querySelectorAll("[class-if]")].forEach(e => {
				const classConditions = JSON.parse(e.getAttribute("class-if"));
				for (const className of classConditions) {
					if (eval(classConditions[className])) {
						e.classList.add(className);
					} else {
						e.classList.remove(className);
					}
				}
			});
			[...document.querySelectorAll("[value-from]")].forEach(e => {
				e.value = eval(e.getAttribute("value-from"));
			});
			[...document.querySelectorAll("[content-from]")].forEach(e => {
				e.textContent = function() { return eval(e.getAttribute("content-from")) }.call(e);
			});
		}

		updateConditionalForms();
		document.addEventListener("input", () => updateConditionalForms());
		document.addEventListener("change", () => updateConditionalForms());
	</script>
{{ end }}
