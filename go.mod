module codeberg.org/codeberg/member-registration

go 1.22

require (
	github.com/snapcore/go-gettext v0.0.0-20230721153050-9082cdc2db05
	github.com/yuin/goldmark v1.4.11
	golang.org/x/text v0.3.8
)
